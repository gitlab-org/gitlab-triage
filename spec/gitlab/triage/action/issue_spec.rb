# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/action/issue'
require 'gitlab/triage/policies/rule_policy'
require 'gitlab/triage/policies_resources/rule_resources'

require 'active_support/core_ext/hash/indifferent_access'

describe Gitlab::Triage::Action::Issue do
  include_context 'with network context'

  let(:resources) do
    [
      { title: 'Issue #0', web_url: 'http://example.com/0', project_id: 123 }
    ]
  end

  let(:policy) do
    Gitlab::Triage::Policies::RulePolicy.new(
      'issues',
      { name: 'Test issue action', actions: actions_hash },
      Gitlab::Triage::PoliciesResources::RuleResources.new(resources),
      network
    )
  end

  let(:actions_hash) do
    {
      issue: {
        title: title,
        description: description
      }
    }
  end

  let(:title) { 'Issue title' }
  let(:description) { "Issue description" }

  subject do
    described_class.new(
      policy: policy, network: network)
  end

  describe '#act' do
    it 'posts the right issue' do
      stub_post = stub_api(
        :post,
        "http://test.com/api/v4/projects/123/issues",
        body: { title: title, description: description }
      )

      subject.act

      assert_requested(stub_post)
    end

    context 'when destination is set' do
      before do
        actions_hash[:issue][:destination] = 'summary/destination'
      end

      it 'posts issue to the right destination project' do
        stub_post = stub_api(
          :post,
          'http://test.com/api/v4/projects/summary%2Fdestination/issues',
          body: { title: title, description: description }
        )

        subject.act

        assert_requested(stub_post)
      end
    end

    context 'when destination is not set' do
      it 'posts issue to the right destination project' do
        stub_post = stub_api(
          :post,
          'http://test.com/api/v4/projects/123/issues',
          body: { title: title, description: description }
        )

        subject.act

        assert_requested(stub_post)
      end
    end

    context 'when there is no resources' do
      let(:resources) { [] }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end

    context 'when issue title is blank' do
      let(:title) { '' }

      it 'does not post anything' do
        expect(subject).not_to receive(:perform)

        subject.act
      end
    end
  end
end
